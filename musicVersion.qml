import QtQuick 2.0
import QtQuick.Controls 1.4
import QtQuick.Dialogs 1.2
import MuseScore 3.0
import FileIO 3.0

MuseScore {
      menuPath: "Plugins.Version Control"
      description: qsTr("Sheet music version control system made by Tamás Sebestyén. This plugin allows you to handle multiple scores in one project file.")
      version: "1.0"
      requiresScore: false
      pluginType: "dock"
      dockArea: "left"    
      width:  400
      
      onRun: {
            if(openedScores > 0) {
                  for(var i=0; i<scores.length; i++) {
                        openedScoresModel.append({
                              scorename: scores[i].scoreName,
                              flag: false
                        })
                  }
            }
      }
      
      property var fileFolder;
      property var fileName;
      property var projectOperation: 0;
      property var jsonObject;
      property var openedScores: scores.length
      property var openedScoreIndex;
      
      Timer {
            id: listener
            interval:1000; running: true; repeat: true
            onTriggered: {
                  if(jsonObject != undefined) {
                        checkScoreFilesModDate()
                        checkScoreFileExists()
                  }
                  if(openedScores < scores.length) {
                        openedScoresModel.append({
                              scorename: scores[openedScores].scoreName,
                              flag: false
                        })
                        openedScores++ 
                  }
                  if(openedScoresModel.count != 0) {
                        for(var i=0;i<scores.length;i++) {
                              if(scores[i].scoreName != openedScoresModel.get(i).scorename) {
                                    openedScoresModel.get(i).scorename = scores[i].scoreName
                                    openedScoresModel.get(i).flag = false
                              }
                        }
                  }
                  if(projectModel.count != 0 && openedScoresModel.count != 0) {
                        for(var i=0; i<openedScoresModel.count;i++) {
                              for(var j=0; j<projectModel.count;j++) {
                                    if(openedScoresModel.get(i).scorename == projectModel.get(j).title) {
                                          openedScoresModel.get(i).flag = true
                                          break   
                                    }      
                              } 
                              if(openedScoresModel.get(i).flag == false) {
                                    listener.running = false
                                    listener.repeat = false
                                    scoreFileDetected.text = qsTr("Score named '" + openedScoresModel.get(i).scorename + "' is opened, but it is not in the project. Would you like to add it?")
                                    openedScoreIndex = i
                                    scoreFileDetected.open()
                                    break
                              }
                        }
                  }
            }
      }
      
      function createProjectFile() {
            if(projectName.text == "" || fileName == undefined) missingNameOrScore.open()
            else {
                  projectModel.clear()
                  
                  scoreFile.source = fileFolder + "/" + fileName
                  
                  if(scoreBranch.text == "") scoreBranch.text = "Master"
                  if(scoreVersion.text == "") scoreVersion.text = "1"
                  projectTitle.text = projectName.text
                  
                  var projectObject = JSON.stringify(
                        {
                        projectname: projectName.text,
                        creationdate: Math.floor(Date.now()/1000),
                        modificationdate: Math.floor(Date.now()/1000), 
                        files: [{
                              title: fileName.substr(0, fileName.length-5),
                              description: scoreDescription.text,
                              branch: scoreBranch.text,
                              tag: scoreTags.text,
                              filename: fileName,
                              version: scoreVersion.text,
                              date: scoreFile.modifiedTime()
                        }]
                        }  
                  )
                  
                  fileName = projectName.text + ".json"
                  
                  outFile.source = fileFolder + "/" + fileName
                  outFile.write(projectObject)                 
                  
                  jsonObject = JSON.parse(projectObject)
                  
                  loadProjectFile()
                  
                  projectDialog.accept()      
            }
      }
      
      function loadProjectFile() {
            if(fileName) {
                  inFile.source = fileFolder + "/" + fileName
                  var json = inFile.read()
                  jsonObject = JSON.parse(json)
                  projectModel.clear()
                       
                  projectTitle.text = jsonObject.projectname
                  projectName.text = jsonObject.projectname
                  
                  function addZero(i) {if (i < 10) {i = "0" + i;} return i;}
                  var months = [qsTr("January"), qsTr("February"), qsTr("March"), qsTr("April"), qsTr("May"), qsTr("June"), qsTr("July"), qsTr("August"), qsTr("September"), qsTr("October"), qsTr("November"), qsTr("December")];
                  var cDate = new Date(jsonObject.creationdate*1000)
                  var mDate = new Date(jsonObject.modificationdate*1000)
                  projectCreationDate.text = qsTr("Created: " + months[cDate.getMonth()] + " " + addZero(cDate.getDate()) + ". " + cDate.getFullYear() + ".")
                  projectModificationDate.text = qsTr("Last modified: " + months[mDate.getMonth()] + " " + addZero(mDate.getDate()) + ". " + mDate.getFullYear() + "., " + addZero(mDate.getHours()) + ":" + addZero(mDate.getMinutes()))
                        
                  var sortBy = [{property:'branch',direction: 1},{property:'version',direction: 1}];
                  
                  jsonObject.files.sort(function(a,b){
                        var i = 0, result = 0;
                        while(i < sortBy.length && result === 0) {
                              result = sortBy[i].direction*(a[ sortBy[i].property ].toString() < b[ sortBy[i].property ].toString() ? -1 : (a[ sortBy[i].property ].toString() > b[ sortBy[i].property ].toString() ? 1 : 0));
                              i++;
                        }
                        return result;
                  })                     
                        
                  for (var i in jsonObject.files) {
                        cDate = new Date(jsonObject.files[i].date*1000)
                        projectModel.append({
                              title: jsonObject.files[i].title, 
                              description : jsonObject.files[i].description, 
                              tag: jsonObject.files[i].tag,
                              branch: jsonObject.files[i].branch + qsTr(" branch","Branch in your language"),
                              filename: jsonObject.files[i].filename,
                              version: "Ver. " + jsonObject.files[i].version,
                              date: cDate.getHours() + ":" + cDate.getMinutes() + ", " + months[cDate.getMonth()] + " " + cDate.getDate() + ". " + cDate.getFullYear() + "."
                              }) 
                        }                               
                  }                                         
      }
      
      function checkScoreFilesModDate() {
            var updateRequired = false
            for(var i in jsonObject.files) {
                  scoreFile.source = fileFolder + "/" + jsonObject.files[i].filename
                  if(scoreFile.modifiedTime() != jsonObject.files[i].date) {
                        jsonObject.files[i].date = scoreFile.modifiedTime()
                        updateRequired = true
                  }
            }
            if(updateRequired) makeJSONFileWOLoad()      
      }
      
      function checkScoreFileExists() {
            var updateRequired = false
            for(var i in jsonObject.files) {
                  scoreFile.source = fileFolder + "/" + jsonObject.files[i].filename
                  if(!scoreFile.exists()) {
                        jsonObject['files'].splice(i,1)
                        updateRequired = true
                  }
            }
            if(updateRequired) {
                  missingScore.open()
                  makeJSONFile()
            }
      }     
      
      function settingElements() {
            bCreateProject.visible = false
            bLoadProject.visible = false
            bUnloadProject.visible = true
            bModifyProject.visible = true
            bDeleteProject.visible = true
            bAddScore.visible = true
            bRemoveScore.visible = true
            bModifyScore.visible = true
      }
      
      function resettingElements() {
            bCreateProject.visible = true
            bLoadProject.visible = true
            bUnloadProject.visible = false
            bModifyProject.visible = false
            bDeleteProject.visible = false
            bAddScore.visible = false
            bRemoveScore.visible = false
            bModifyScore.visible = false
            bMusicFile.visible = true 
            projectName.readOnly = false
            projectOperation = 0; 
            projectTitle.text = qsTr("Please load or create a project")
            projectDialog.title = qsTr("Creating project...")
            bSubmit.text = qsTr("Create project")
            projectTitle.text = qsTr("Please load or create a project")
            projectName.text = ""
            projectCreationDate.text = ""
            projectModificationDate.text = ""
            projectName.textColor = "black"
            scoreDescription.text = ""
            scoreBranch.text = ""
            scoreTags.text = ""
            scoreVersion.text = ""
            projectDialog.title = qsTr("Creating project...")
            projectModel.clear()
            fileFolder = undefined
            fileName = undefined
            jsonObject = undefined
            openedScoreIndex = undefined
            if(scores.length > 0) {for(var i=0; i<scores.length; i++) {openedScoresModel.get(i).flag = false}}            
      }
      
      //Make the JSON with loading it into the model
      function makeJSONFile() {
            var projectObject = JSON.stringify(jsonObject)
            fileName = projectName.text + ".json"
            outFile.source = fileFolder + "/" + fileName
            outFile.write(projectObject)
            jsonObject = JSON.parse(projectObject)           
            loadProjectFile()         
      }
      
      //Make the JSON without loading it into the model
      function makeJSONFileWOLoad() {
            var projectObject = JSON.stringify(jsonObject)
            fileName = projectName.text + ".json"
            outFile.source = fileFolder + "/" + fileName
            outFile.write(projectObject)
            jsonObject = JSON.parse(projectObject)
      }
      
      function modifyProject() {
            if(projectRename.text == "") projectModifyDialog.reject()
            projectName.text = projectRename.text       
            inFile.remove()         
            jsonObject.projectname = projectName.text 
            jsonObject.modificationdate = Math.floor(Date.now()/1000)        
            makeJSONFile()                              
            projectModifyDialog.accept()
      }
      
      function addScoreToProject() {                            
            if(scoreBranch.text == "") scoreBranch.text = qsTr("Master","Master branch's name in your language") 
            if(scoreVersion.text == "") scoreVersion.text = "-"
            jsonObject.modificationdate = Math.floor(Date.now()/1000)
            scoreFile.source = fileFolder + "/" + fileName             
            jsonObject['files'].push({
                  "title": fileName.substr(0, fileName.length-5),
                  "description": scoreDescription.text,
                  "branch": scoreBranch.text,
                  "tag": scoreTags.text,
                  "filename": fileName,
                  "version": scoreVersion.text,
                  "date": scoreFile.modifiedTime() 
            })  
            makeJSONFile()                               
            projectDialog.accept()
      }
      
      function addScoreToProjectFromTab() {
            if(scoreBranch.text == "") scoreBranch.text = qsTr("Master","Name of the master branch")
            if(scoreVersion.text == "") scoreVersion.text = "-"
            jsonObject.modificationdate = Math.floor(Date.now()/1000)
            fileName = scores[openedScoreIndex].scoreName + ".mscz"
            scoreFile.source = fileFolder + "/" + fileName
            if(scoreFile.exists()) {
                  jsonObject['files'].push({
                        "title": fileName.substr(0, fileName.length-5),
                        "description": scoreDescription.text,
                        "branch": scoreBranch.text,
                        "tag": scoreTags.text,
                        "filename": fileName,
                        "version": scoreVersion.text,
                        "date": scoreFile.modifiedTime() 
                  })  
                  makeJSONFile()                               
                  projectDialog.accept()   
            }
            else {
                  sameDirectory2.open()
                  projectDialog.reject()
            } 
            listener.running = true
            listener.repeat = true
      }
      
      function removeScoreFromProject() {
            jsonObject.modificationdate = Math.floor(Date.now()/1000)
            var selectedScore = projectModel.get(sheetView.currentIndex.row)
            var selectedScoreIndex
            jsonObject['files'].find(function(item, i) {
                  if(item.title == selectedScore.title) {
                        selectedScoreIndex = i
                  }
            })
            scoreFile.source = fileFolder + "/" + selectedScore.filename
            removeScoreFile.open()
            jsonObject['files'].splice(selectedScoreIndex,1)
            makeJSONFile()         
      }
      
      function modifyScoreInProject() {
            jsonObject.modificationdate = Math.floor(Date.now()/1000)
            var selectedScore = projectModel.get(sheetView.currentIndex.row)
            var selectedScoreIndex
            jsonObject['files'].find(function(item, i) {
                  if(item.title == selectedScore.title) {
                        selectedScoreIndex = i
                  }
            })
            jsonObject.files[selectedScoreIndex].description = scoreDescription.text
            jsonObject.files[selectedScoreIndex].branch = scoreBranch.text
            jsonObject.files[selectedScoreIndex].tag = scoreTags.text
            jsonObject.files[selectedScoreIndex].version = scoreVersion.text
            makeJSONFile()
            projectDialog.accept()
      }
      
      MessageDialog {
            id: missingNameOrScore
            title: qsTr("Error while creating the project")
            text: qsTr("Project name or sheet music file is missing. Please specify them.")
            standardButtons: StandardButton.Ok
      }
      
      MessageDialog {
            id: sameDirectory
            title: qsTr("Error while adding score file")
            text: qsTr("The project file and the newly added score must be in the same directory!")
            standardButtons: StandardButton.Ok
            onAccepted: {
                  musicFileDialog.open()
            }
      }
      
      MessageDialog {
            id: sameDirectory2
            title: qsTr("Error while adding score file")
            text: qsTr("The project file and the newly added score must be in the same directory!")
            standardButtons: StandardButton.Ok
      }
      
      MessageDialog {
            id: sameScore
            title: qsTr("Error while adding score file")
            text: qsTr("This score is already in the project. Please select another score.")
            standardButtons: StandardButton.Ok
            onAccepted: {
                  musicFileDialog.open()
            }
      }
      
      MessageDialog {
            id: missingScore
            title: qsTr("Missing score file")
            text: qsTr("One or more of the project's score file(s) is/are missing. This/these score(s) has been removed from the project.")
            standardButtons: StandardButton.Ok
      }
      
      MessageDialog {
            id: deleteProject
            title: qsTr("Deleting project")
            text: qsTr("Are you sure you want to delete the project?")
            standardButtons: StandardButton.Yes | StandardButton.No
            onYes: removeScoreFiles.open()                 
      }
      
      MessageDialog {
            id: selectScoreError
            title: qsTr("Error while selecting a score from the project")
            text: qsTr("There is no score selected. In order to execute this function, you must first select a score from the list!")
            standardButtons: StandardButton.Ok
      }
      
      MessageDialog {
            id: removeScore
            title: qsTr("Removing score")
            text: qsTr("Are you sure you want to remove the selected score from the project?")
            standardButtons: StandardButton.Yes | StandardButton.No
            onYes: {
                  removeScoreFromProject()    
            }
      }
      
      MessageDialog {
            id: removeScoreFile
            title: qsTr("Removing score file")
            text: qsTr("You want to delete the score file as well?")
            standardButtons: StandardButton.Yes | StandardButton.No
            onYes: scoreFile.remove()
      }
      
      MessageDialog {
            id: scoreFileDetected
            title: qsTr("Score file detected")
            standardButtons: StandardButton.Yes | StandardButton.No
            onYes: {
                  bSubmit.text = qsTr("Add score")
                  projectDialog.modality
                  projectName.readOnly = true
                  projectOperation = 3  
                  projectName.textColor = "#a1a1a1"
                  bMusicFile.visible = false
                  projectDialog.title = qsTr("Adding score")
                  projectDialog.open()
                  openedScoresModel.get(openedScoreIndex).flag = true
            }
            onNo: {
                  openedScoresModel.get(openedScoreIndex).flag = true
                  listener.running = true
                  listener.repeat = true
            }
      }
      
      MessageDialog {
            id: removeScoreFiles
            title: qsTr("Removing score files")
            text: qsTr("You want to delete the score files as well?")
            standardButtons: StandardButton.Yes | StandardButton.No
            onYes: {
                  for(var i in jsonObject.files) {
                        scoreFile.source = fileFolder + "/" + jsonObject.files[i].filename
                        scoreFile.remove()
                  }
                  resettingElements()
                  inFile.remove()
            }
            onNo: {
                  resettingElements()
                  inFile.remove()
            }
      }
      
      Button {
            id: bCreateProject
            text: qsTr("Create project")
            width: 100
            onClicked: {
                  projectDialog.modality
                  projectDialog.open()
                  loadProjectFile()
                  }
            y: 555
            x: 10
      }
      
      Button {
            id: bLoadProject
            text: qsTr("Load project")
            width: 100
            onClicked: projectFileDialog.open();
            y: 595
            x: 10
      }

      Button {
            id: bModifyProject
            visible: false
            text: qsTr("Modify project")
            width: 100
            onClicked: {
                  projectRename.text = projectTitle.text
                  projectModifyDialog.modality
                  projectModifyDialog.open()
            }
            y: 555
            x: 10
      }            
                  
      Button {
            id: bUnloadProject
            visible: false
            text: qsTr("Unload project")
            width: 100
            onClicked: {
                  resettingElements()
            }
            y: 595
            x: 10
      }
      
      Button {
            id: bDeleteProject
            visible: false
            text: qsTr("Delete project")
            width: 100
            onClicked: deleteProject.open()                  
            y: 635
            x: 10
      }
            
      Button {
            id: bAddScore
            visible: false
            text: qsTr("Add score")
            width: 100
            onClicked: {
                  bSubmit.text = bAddScore.text
                  projectDialog.modality
                  projectName.readOnly = true
                  projectOperation = 1
                  projectName.textColor = "#a1a1a1"
                  bMusicFile.visible = true
                  projectDialog.title = qsTr("Adding score...")
                  projectDialog.open();      
            }
            y: 555
            x: 200
      }       
      
      Button {
            id: bRemoveScore
            visible: false
            text: qsTr("Remove score")
            width: 100
            onClicked: {
                  if(projectModel.get(sheetView.currentIndex.row)==undefined) selectScoreError.open()      
                  else removeScore.open()
            }
            y: 595
            x: 200
      }       
      
      Button {
            id: bModifyScore
            visible: false
            text: qsTr("Modify score")
            width: 100
            onClicked: {
                  var selectedScore = projectModel.get(sheetView.currentIndex.row)
                  if(selectedScore==undefined) selectScoreError.open()
                  else {
                        bSubmit.text = bModifyScore.text
                        projectDialog.modality
                        projectName.readOnly = true
                        projectOperation = 2  
                        projectName.textColor = "#a1a1a1"
                        bMusicFile.visible = false
                        scoreDescription.text = selectedScore.description
                        scoreBranch.text = selectedScore.branch.substr(0, selectedScore.branch.length-7)
                        scoreTags.text = selectedScore.tag
                        scoreVersion.text = selectedScore.version.substr(5,1)
                        projectDialog.title = qsTr("Modify score")
                        projectDialog.open()
                  }      
            }
            y: 635
            x: 200
      }       
                  
      FileIO {id: inFile}
        
      FileIO {id: outFile}
      
      FileIO {id: scoreFile}        
      
      Dialog {
            id: projectDialog
            title: qsTr("Creating project...")
            modality : Qt.ApplicationModal
            onAccepted: {
                  settingElements()
            }
            
            contentItem: Rectangle {
                  implicitWidth: 480
                  implicitHeight: 260
                  color: "lightgrey"
                  
                  Label {
                        text: qsTr("Name the project")
                        font.bold: true
                        x: 10
                        y: 10  
                  }
                  TextField {
                        id: projectName
                        width: 150
                        x: 320
                        y: 10
                  } 
                  Label {
                        id: musicFileDialogLabel 
                        text: qsTr("Specify the score")
                        font.bold: true
                        x: 10
                        y: 40  
                  }
                  Button {
                        id: bMusicFile
                        text: qsTr("Load sheet music...")
                        width: 150
                        onClicked: musicFileDialog.open();
                        x: 320
                        y: 40
                  }
                  Label {
                        text: qsTr("Add a description for this score")
                        x: 20
                        y: 70  
                  }
                  TextField {
                        id: scoreDescription
                        width: 150
                        x: 320
                        y: 70
                  }
                  Label {
                        text: qsTr("Add (a) tag(s) to this score")
                        x: 20
                        y: 100  
                  }
                  TextField {
                        id: scoreTags
                        width: 150
                        x: 320
                        y: 100
                  }
                  Label {
                        text: qsTr("Branch","Score's branch")
                        x: 20
                        y: 130  
                  }
                  TextField {
                        id: scoreBranch
                        width: 150
                        x: 320
                        y: 130
                  }
                  Label {
                        text: qsTr("Version","Score's version")
                        x: 20
                        y: 160  
                  }
                  TextField {
                        id: scoreVersion
                        width: 150
                        x: 320
                        y: 160
                  }
                  Button {
                        id: bSubmit
                        text: "Create project"
                        width: 150
                        onClicked: {                       
                              if(projectOperation == 0) createProjectFile()
                              if(projectOperation == 1) addScoreToProject()
                              if(projectOperation == 2) modifyScoreInProject()
                              if(projectOperation == 3) addScoreToProjectFromTab()
                        }
                        x: 30
                        y: 220
                  }
                  Button {
                        text: qsTr("Cancel")
                        width: 150
                        onClicked: {
                              projectDialog.close()
                              listener.running = true
                              listener.repeat = true
                        }
                        x: 300
                        y: 220
                  }  
            }     
      }
      
      Dialog {
            id: projectModifyDialog
            title: qsTr("Modify project")
            modality : Qt.ApplicationModal
            
            contentItem: Rectangle {
                  implicitWidth: 480
                  implicitHeight: 90
                  color: "lightgrey"
                  
                  Label {
                        text: qsTr("Rename the project")
                        font.bold: true
                        x: 10
                        y: 10  
                  }
                  TextField {
                        id: projectRename
                        width: 150
                        x: 320
                        y: 10
                  }                                                
                  Button {
                        id: bProjectModifySubmit
                        text: qsTr("Modify project")
                        width: 150
                        onClicked: modifyProject()
                        x: 30
                        y: 50
                  }
                  Button {
                        text: qsTr("Cancel")
                        width: 150
                        onClicked: projectModifyDialog.close()
                        x: 300
                        y: 50
                  }
            }
      } 
                      
      FileDialog {
            id: projectFileDialog
            title: qsTr("Please choose a project file")
            nameFilters: ["Project files (*.json)"]
            onAccepted: {
                  fileFolder = String(projectFileDialog.folder)
                  fileName = String(projectFileDialog.fileUrl)
                  fileName = fileName.substring(fileFolder.length+1, fileName.length)
                  fileFolder = fileFolder.substring(8, fileFolder.length)
                  settingElements()
                  loadProjectFile()                                              
            }                                               
      }                                                    
      
      FileDialog {
            id: musicFileDialog
            title: qsTr("Please choose a Sheet music file")
            nameFilters: ["MuseScore files (*.mscz)", "MusicXML (*.mxml)", "GuitarPro files (*.gpx)"]
            onAccepted: {
                  if(projectOperation==1) {
                        var tmpFileFolder = String(musicFileDialog.folder)
                        tmpFileFolder = tmpFileFolder.substring(8, tmpFileFolder.length)
                        if(fileFolder != tmpFileFolder) {
                              sameDirectory.open()
                        } 
                        else {
                              var tmpFileName = String(musicFileDialog.fileUrl)
                              tmpFileName = tmpFileName.substring(tmpFileFolder.length+9, tmpFileName.length)
                              var sameScoreFile = false
                              for(var i in jsonObject.files) {
                                    if(jsonObject.files[i].filename == tmpFileName) {
                                          sameScoreFile = true
                                    }      
                              }
                              if(sameScoreFile) {
                                    sameScore.open()
                              }
                              else {
                                    fileFolder = String(musicFileDialog.folder)
                                    fileName = String(musicFileDialog.fileUrl)
                                    fileName = fileName.substring(fileFolder.length+1, fileName.length)
                                    fileFolder = fileFolder.substring(8, fileFolder.length)
                              }
                        }
                  }
                  else {
                        fileFolder = String(musicFileDialog.folder)
                        fileName = String(musicFileDialog.fileUrl)
                        fileName = fileName.substring(fileFolder.length+1, fileName.length)
                        fileFolder = fileFolder.substring(8, fileFolder.length)
                  }
            }
      }         
      
      ListModel {id: projectModel}
      
      ListModel {id: openedScoresModel}    
            
      Component {
            id: sectionHeading
            Rectangle {
                  height: childrenRect.height *1.1
                  Text {
                        text: section
                        font.bold: true
                        color: "black"
                  }
            }
      }      
      
      Text {
            id: projectTitle
            font.bold: true
            font.letterSpacing: 4
            text: qsTr("Please load or create a project")
            x: 5
      }
      
      Text {
            id: projectCreationDate
            font.letterSpacing: 1.5
            x: 5
            y: 13
      }
	  
	  Text {
            id: projectModificationDate
            font.letterSpacing: 1.5
            x: 5
            y: 26
      }
            
      TreeView {
            section.property: "branch"
            section.criteria: ViewSection.FullString
            section.delegate: sectionHeading
            TableViewColumn {
                  title: qsTr("Title","One of the columns in the table, which representint the scores")
                  role: "title"
                  width: 150
                  }
            TableViewColumn {
                  title: qsTr("Version","One of the columns in the table, which representint the scores")
                  role: "version"
                  width: 50
                  }
            TableViewColumn {
                  title: qsTr("Description","One of the columns in the table, which representint the scores")
                  role: "description"
                  width: 300
                  }
            TableViewColumn {
                  title: qsTr("Tag(s)","One of the columns in the table, which representint the scores")
                  role: "tag"
                  width: 100
                  }
             TableViewColumn {
                  title: qsTr("Date","One of the columns in the table, which representint the scores")
                  role: "date"
                  width: 200
                  }                    
             onDoubleClicked: {
                  fileName = projectModel.get(currentIndex.row).filename
                  readScore( fileFolder + "/" + fileName, false); 
                  }   
            model: projectModel
            id: sheetView
            width: parent.width-20
            height: 500  
            x: 10
            y: 45  
      }           
}